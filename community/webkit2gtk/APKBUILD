# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Contributor: Sergei Lukin <sergej.lukin@gmail.com>
# Contributor: Jiri Horner <laeqten@gmail.com>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=webkit2gtk
pkgver=2.40.0
pkgrel=1
pkgdesc="Portable web rendering engine WebKit for GTK+"
url="https://webkitgtk.org/"
arch="all"
license="LGPL-2.0-or-later AND BSD-2-Clause"
depends="bubblewrap xdg-dbus-proxy dbus:org.freedesktop.Secrets"
makedepends="
	bison
	clang
	cmake
	enchant2-dev
	flex
	flite-dev
	geoclue-dev
	gnutls-dev
	gobject-introspection-dev
	gperf
	gst-plugins-bad-dev
	gst-plugins-base-dev
	gstreamer-dev
	gtk+3.0-dev
	hyphen-dev
	icu-dev
	lcms2-dev
	libavif-dev
	libgcrypt-dev
	libjpeg-turbo-dev
	libmanette-dev
	libpng-dev
	libseccomp-dev
	libsecret-dev
	libsoup-dev
	libwebp-dev
	libwpe-dev
	libwpebackend-fdo-dev
	libxml2-dev
	libxslt-dev
	libxt-dev
	llvm
	mesa-dev
	openjpeg-dev
	pango-dev
	python3
	ruby
	samurai
	sqlite-dev
	unifdef
	woff2-dev
	"
replaces="webkit"
options="!check" # upstream doesn't package them in release tarballs: Tools/Scripts/run-gtk-tests: Command not found
subpackages="$pkgname-dev $pkgname-lang $pkgname-dbg"
source="https://webkitgtk.org/releases/webkitgtk-$pkgver.tar.xz
	initial-exec.patch
	riscv-fix-1.patch
	riscv-fix-2.patch
	riscv-fix-3.patch
	riscv-fix-4.patch
	riscv-fix-5.patch
	riscv-fix-6.patch
	riscv-fix-7.patch
	"
builddir="$srcdir/webkitgtk-$pkgver"

# secfixes:
#   2.36.5-r0:
#     - CVE-2022-2294
#     - CVE-2022-32792
#     - CVE-2022-32816
#   2.36.4-r0:
#     - CVE-2022-22677
#     - CVE-2022-22710
#   2.36.1-r0:
#     - CVE-2022-30293
#     - CVE-2022-30294
#   2.36.0-r0:
#     - CVE-2022-22624
#     - CVE-2022-22628
#     - CVE-2022-22629
#   2.34.6-r0:
#     - CVE-2022-22589
#     - CVE-2022-22590
#     - CVE-2022-22592
#     - CVE-2022-22620
#   2.34.4-r0:
#     - CVE-2021-30934
#     - CVE-2021-30936
#     - CVE-2021-30951
#     - CVE-2021-30952
#     - CVE-2021-30953
#     - CVE-2021-30954
#     - CVE-2021-30984
#     - CVE-2022-22637
#     - CVE-2022-22594
#   2.34.3-r0:
#     - CVE-2021-30887
#     - CVE-2021-30890
#   2.34.1-r0:
#     - CVE-2021-42762
#   2.34.0-r0:
#     - CVE-2021-30818
#     - CVE-2021-30823
#     - CVE-2021-30846
#     - CVE-2021-30851
#     - CVE-2021-30884
#     - CVE-2021-30888
#     - CVE-2021-30889
#     - CVE-2021-30897
#     - CVE-2021-45481
#     - CVE-2021-45483
#   2.32.4-r0:
#     - CVE-2021-30809
#     - CVE-2021-30836
#     - CVE-2021-30848
#     - CVE-2021-30849
#     - CVE-2021-30858
#     - CVE-2021-45482
#   2.32.3-r0:
#     - CVE-2021-21775
#     - CVE-2021-21779
#     - CVE-2021-30663
#     - CVE-2021-30665
#     - CVE-2021-30689
#     - CVE-2021-30720
#     - CVE-2021-30734
#     - CVE-2021-30744
#     - CVE-2021-30749
#     - CVE-2021-30795
#     - CVE-2021-30797
#     - CVE-2021-30799
#   2.32.2-r0:
#     - CVE-2021-30758
#   2.32.0-r0:
#     - CVE-2021-1788
#     - CVE-2021-1844
#     - CVE-2021-1871
#     - CVE-2021-30682
#   2.30.6-r0:
#     - CVE-2020-27918
#     - CVE-2020-29623
#     - CVE-2021-1765
#     - CVE-2021-1789
#     - CVE-2021-1799
#     - CVE-2021-1801
#     - CVE-2021-1870
#     - CVE-2021-21806
#   2.30.5-r0:
#     - CVE-2020-9947
#     - CVE-2020-13558
#   2.30.3-r0:
#     - CVE-2020-9983
#     - CVE-2020-13543
#     - CVE-2020-13584
#   2.30.0-r0:
#     - CVE-2020-9948
#     - CVE-2020-9951
#     - CVE-2021-1817
#     - CVE-2021-1820
#     - CVE-2021-1825
#     - CVE-2021-1826
#     - CVE-2021-30661
#   2.28.4-r0:
#     - CVE-2020-9862
#     - CVE-2020-9893
#     - CVE-2020-9894
#     - CVE-2020-9895
#     - CVE-2020-9915
#     - CVE-2020-9925
#   2.28.3-r0:
#     - CVE-2020-13753
#     - CVE-2020-9802
#     - CVE-2020-9803
#     - CVE-2020-9805
#     - CVE-2020-9806
#     - CVE-2020-9807
#     - CVE-2020-9843
#     - CVE-2020-9850
#     - CVE-2020-9952
#   2.28.1-r0:
#     - CVE-2020-11793
#   2.28.0-r0:
#     - CVE-2020-10018
#     - CVE-2021-30762
#   2.26.3-r0:
#     - CVE-2019-8835
#     - CVE-2019-8844
#     - CVE-2019-8846
#   2.26.2-r0:
#     - CVE-2019-8812
#     - CVE-2019-8814
#   2.26.1-r0:
#     - CVE-2019-8783
#     - CVE-2019-8811
#     - CVE-2019-8813
#     - CVE-2019-8816
#     - CVE-2019-8819
#     - CVE-2019-8820
#     - CVE-2019-8823
#   2.26.0-r0:
#     - CVE-2019-8625
#     - CVE-2019-8710
#     - CVE-2019-8720
#     - CVE-2019-8743
#     - CVE-2019-8764
#     - CVE-2019-8766
#     - CVE-2019-8769
#     - CVE-2019-8771
#     - CVE-2019-8782
#     - CVE-2019-8815
#     - CVE-2021-30666
#     - CVE-2021-30761
#   2.24.4-r0:
#     - CVE-2019-8674
#     - CVE-2019-8707
#     - CVE-2019-8719
#     - CVE-2019-8733
#     - CVE-2019-8763
#     - CVE-2019-8765
#     - CVE-2019-8768
#     - CVE-2019-8821
#     - CVE-2019-8822
#   2.24.3-r0:
#     - CVE-2019-8644
#     - CVE-2019-8649
#     - CVE-2019-8658
#     - CVE-2019-8666
#     - CVE-2019-8669
#     - CVE-2019-8671
#     - CVE-2019-8672
#     - CVE-2019-8673
#     - CVE-2019-8676
#     - CVE-2019-8677
#     - CVE-2019-8678
#     - CVE-2019-8679
#     - CVE-2019-8680
#     - CVE-2019-8681
#     - CVE-2019-8683
#     - CVE-2019-8684
#     - CVE-2019-8686
#     - CVE-2019-8687
#     - CVE-2019-8688
#     - CVE-2019-8689
#     - CVE-2019-8690
#     - CVE-2019-8726
#   2.24.2-r0:
#     - CVE-2019-8735
#   2.24.1-r0:
#     - CVE-2019-6251
#     - CVE-2019-8506
#     - CVE-2019-8524
#     - CVE-2019-8535
#     - CVE-2019-8536
#     - CVE-2019-8544
#     - CVE-2019-8551
#     - CVE-2019-8558
#     - CVE-2019-8559
#     - CVE-2019-8563
#     - CVE-2019-11070
#   2.22.7-r0:
#     - CVE-2018-4437
#     - CVE-2019-6212
#     - CVE-2019-6215
#     - CVE-2019-6216
#     - CVE-2019-6217
#     - CVE-2019-6227
#     - CVE-2019-6229
#   2.22.4-r0:
#     - CVE-2018-4372
#   2.18.4-r0:
#     - CVE-2017-7156
#     - CVE-2017-7157
#     - CVE-2017-13856
#     - CVE-2017-13866
#     - CVE-2017-13870
#   2.14.5-r0:
#     - CVE-2017-2350
#     - CVE-2017-2354
#     - CVE-2017-2355
#     - CVE-2017-2356
#     - CVE-2017-2362
#     - CVE-2017-2363
#     - CVE-2017-2364
#     - CVE-2017-2365
#     - CVE-2017-2366
#     - CVE-2017-2369
#     - CVE-2017-2371
#     - CVE-2017-2373

case "$CARCH" in
s390x)
	;;
*)
	makedepends="$makedepends lld"
	;;
esac

build() {
	case "$CARCH" in
	s390x|armhf|armv7|x86|ppc64le)
		# llint/LowLevelInterpreter.cpp fails to build with fortify source here
		export CXXFLAGS="$CXXFLAGS -U_FORTIFY_SOURCE"
		;;
	esac

	case "$CARCH" in
	armv7)
		# clang fails to build armv7 due to some NEON related thing.
		# https://github.com/WebKit/WebKit/pull/1233 should fix it
		;;
	s390x)
		# no lld on s390x
		export CC=clang
		export CXX=clang++
		;;
	*)
		# the debug symbols become 1/2 the size, and actual webkit becomes
		# smaller too.
		export CC=clang
		export CXX=clang++
		export LDFLAGS="$LDFLAGS -fuse-ld=lld"
		;;
	esac

	case "$CARCH" in
	arm*|aarch64|s390x|riscv64)
		# arm: seemingly broken?
		# s390x/riscv64: no lld
		;;
	*)
		local lto="-DLTO_MODE=thin"
		;;
	esac

	export AR=llvm-ar
	export NM=llvm-nm
	export RANLIB=llvm-ranlib

	# significantly reduce debug symbol size
	export CFLAGS="$CFLAGS -g1"
	export CXXFLAGS="$CXXFLAGS -g1"

	# sampling profiler is broken on musl
	cmake -B build -G Ninja \
		-DPORT=GTK \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_SKIP_RPATH=ON \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DENABLE_DOCUMENTATION=OFF \
		-DENABLE_JOURNALD_LOG=OFF \
		-DENABLE_MINIBROWSER=ON \
		-DENABLE_SAMPLING_PROFILER=OFF \
		-DUSE_SOUP2=ON \
		$lto
	cmake --build build
}

check() {
	ninja -C build check
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

dbg() {
	# hack to use over binutils objcopy for riscv
	CROSS_COMPILE=llvm-
	default_dbg
}

sha512sums="
550dafb31d71edf984d6b7636366f839d31f6b244b87f83c6efe7af17efe3f2f63268d2c39a6a3c474afadf2b30df868efaefcf38197b30cc6b11c63de7d2ddc  webkitgtk-2.40.0.tar.xz
26f3df81758068a83bf770e1f8b48546e9ec2428d23cbc4e1c5cc7851c91ad1dfeeac89aea73568a5f498cd6c053aaab7e1af67e59a471ad2d0375c1c64cbd8a  initial-exec.patch
8ddd2cb2a10aa4c9296ae641e15ff8b58fd48c9fd9ccc5a4b2ebc40c4a90e3f2cffa3d9e030b0038a169d97f463b62d64707caa2c64eaea21cfce0f0a04d29d4  riscv-fix-1.patch
a5a5d62aea820c087f7919b617bef5adefa6f34a8189fe1993250535ff59585bed4c6fd24d69caaeb814dcaa449194dd533886a41e70acceb3882f8399494404  riscv-fix-2.patch
5f9c44fd9b29587110208ad63e845eb178cc517a56e18bd0a4c17acc5dfa772c6b92bacad219b9e1910e030e019b125a7ec523f85af013bd53f05e4e30e595cb  riscv-fix-3.patch
1c33fa822a245f0f8db1caa3b368d3dd56e2595e509fd422d7d888bbe23288426907a4dd4fc0e14a1d28ff3c3240cb69e4ef8a6326ec27eb1db50c31b84da006  riscv-fix-4.patch
ba730685aee231d0941229601e4dedc70c53737c7702d9b58600f9bf4eff793490b218f042750145363902570a0915da720726bd969092b402d03413536db714  riscv-fix-5.patch
e7193564d415c4c71d735e897ce74b6efa49eb43060e8a44a494e854ae67099588bc982bce1a6ced27726589e2f93fc2e1ff8c5bb99b3444cb7d479c31b233e0  riscv-fix-6.patch
15c34e6fc59279627ea6801d8954a95c5d8ba711cfb6ae7aad1335739675940a244137c6b1a055ae6f7c8017da07430d9b1b9a4b789cdc4c9072b585a4573bd1  riscv-fix-7.patch
"

# Contributor: Jake Buchholz Göktürk <tomalok@gmail.com>
# Maintainer: Jake Buchholz Göktürk <tomalok@gmail.com>

pkgname=runc
pkgdesc="CLI tool for spawning and running containers according to the OCI specification"
url="https://www.opencontainers.org"
_commit=f19387a6bec4944c770f7668ab51c4348d9c2f38
pkgver=1.1.5
pkgrel=1
arch="all"
license="Apache-2.0"
makedepends="bash go go-md2man libseccomp-dev libtool"
subpackages="$pkgname-doc"
source="https://github.com/opencontainers/runc/archive/v$pkgver/runc-$pkgver.tar.gz
	"
options="!check"

provides="oci-runtime"
provider_priority=90

# secfixes:
#   1.1.4-r0:
#     - CVE-2023-25809
#     - CVE-2023-27561
#     - CVE-2023-28642
#   1.1.4-r7:
#     - CVE-2023-27561
#   1.1.2-r0:
#     - CVE-2022-29162
#   1.0.3-r0:
#     - CVE-2021-43784
#   1.0.0_rc95-r0:
#     - CVE-2021-30465
#   1.0.0_rc10-r0:
#     - CVE-2019-19921
#   1.0.0_rc9-r0:
#     - CVE-2019-16884
#   1.0.0_rc7-r0:
#     - CVE-2019-5736

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	make COMMIT="$_commit"
	make man
}

package() {
	install -Dsm755 runc "$pkgdir"/usr/bin/runc
	install -Dm644 "$builddir"/man/man8/* -t "$pkgdir"/usr/share/man/man8/
}

sha512sums="
f3cc9b93b0fe8a4341d410010fe584febb8e975ec9e0fd569d7dff33ab74c5821a2e0c40b7aeafd6b90991a50eae9c352342437f09cf6884dc850ceccdc68944  runc-1.1.5.tar.gz
"

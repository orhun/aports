# Contributor: David Demelier <markand@malikania.fr>
# Maintainer: David Demelier <markand@malikania.fr>
pkgname=wpewebkit
pkgver=2.40.0
pkgrel=1
pkgdesc="WebKit port optimized for embedded devices"
url="https://wpewebkit.org"
arch="all"
license="other"
makedepends="
	at-spi2-core-dev
	bubblewrap
	cairo-dev
	clang
	cmake
	gi-docgen
	gobject-introspection-dev
	gperf
	gst-plugins-bad-dev
	gst-plugins-base-dev
	gtk-doc
	harfbuzz-dev
	lcms2-dev
	libavif-dev
	libepoxy-dev
	libgcrypt-dev
	libseccomp-dev
	libsoup3-dev
	libtasn1-dev
	libwpe-dev
	libwpebackend-fdo-dev
	libxkbcommon-dev
	libxslt-dev
	llvm
	ninja
	openjpeg-dev
	perl
	ruby-dev
	unifdef
	wayland-dev
	wayland-protocols
	woff2-dev
	xdg-dbus-proxy
	"
subpackages="$pkgname-dev $pkgname-doc"
source="https://wpewebkit.org/releases/wpewebkit-$pkgver.tar.xz
	initial-exec.patch
	patch-gettext.patch
	riscv-fix-1.patch
	riscv-fix-2.patch
	riscv-fix-3.patch
	riscv-fix-4.patch
	riscv-fix-5.patch
	riscv-fix-6.patch
	"
# fails to strip on riscv64 with binutils
options="!check !strip"

case "$CARCH" in
s390x)
	;;
*)
	makedepends="$makedepends lld"
	;;
esac

build() {
	case "$CARCH" in
	s390x|armhf|armv7|x86|ppc64le)
		# llint/LowLevelInterpreter.cpp fails to build with fortify source here
		export CXXFLAGS="$CXXFLAGS -U_FORTIFY_SOURCE"
		;;
	esac

	case "$CARCH" in
	armv7)
		# clang fails to build armv7 due to some NEON related thing.
		# https://github.com/WebKit/WebKit/pull/1233 should fix it
		;;
	s390x)
		# no lld on s390x
		export CC=clang
		export CXX=clang++
		;;
	*)
		# much lower build memory usage and better final size
		export CC=clang
		export CXX=clang++
		export LDFLAGS="$LDFLAGS -fuse-ld=lld"
		;;
	esac
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi

	export AR=llvm-ar
	export NM=llvm-nm
	export RANLIB=llvm-ranlib

	cmake -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=Release \
		-DPORT=WPE \
		-DENABLE_MINIBROWSER=On \
		-DENABLE_JOURNALD_LOG=Off \
		-GNinja \
		$CMAKE_CROSSOPTS .
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	# binutils can't strip the binaries on riscv, so use the llvm binutils for
	# it by hand
	scanelf --recursive \
		--nobanner \
		--etype "ET_DYN,ET_EXEC" \
		--format "%F" \
		"$pkgdir" \
		| while read -r file; do
		llvm-strip "$file"
	done
}

sha512sums="
83db18d7897525cf89fa61bf7237c7f058b82247cf62f29df5b3b1da84323477923b298269ab76a7c07c02e9d1df7e77d1d81397500f9d2c3b6d549002ff39df  wpewebkit-2.40.0.tar.xz
26f3df81758068a83bf770e1f8b48546e9ec2428d23cbc4e1c5cc7851c91ad1dfeeac89aea73568a5f498cd6c053aaab7e1af67e59a471ad2d0375c1c64cbd8a  initial-exec.patch
4316330f0c42fcfe800210bdbeabbb6bdcf532b71e2761550b8a753499d801fd0405cc961a516dfddfc28c3a6cf0c17b6db461ff51158238b8d874bf75b799f2  patch-gettext.patch
8ddd2cb2a10aa4c9296ae641e15ff8b58fd48c9fd9ccc5a4b2ebc40c4a90e3f2cffa3d9e030b0038a169d97f463b62d64707caa2c64eaea21cfce0f0a04d29d4  riscv-fix-1.patch
a5a5d62aea820c087f7919b617bef5adefa6f34a8189fe1993250535ff59585bed4c6fd24d69caaeb814dcaa449194dd533886a41e70acceb3882f8399494404  riscv-fix-2.patch
5f9c44fd9b29587110208ad63e845eb178cc517a56e18bd0a4c17acc5dfa772c6b92bacad219b9e1910e030e019b125a7ec523f85af013bd53f05e4e30e595cb  riscv-fix-3.patch
1c33fa822a245f0f8db1caa3b368d3dd56e2595e509fd422d7d888bbe23288426907a4dd4fc0e14a1d28ff3c3240cb69e4ef8a6326ec27eb1db50c31b84da006  riscv-fix-4.patch
ba730685aee231d0941229601e4dedc70c53737c7702d9b58600f9bf4eff793490b218f042750145363902570a0915da720726bd969092b402d03413536db714  riscv-fix-5.patch
e7193564d415c4c71d735e897ce74b6efa49eb43060e8a44a494e854ae67099588bc982bce1a6ced27726589e2f93fc2e1ff8c5bb99b3444cb7d479c31b233e0  riscv-fix-6.patch
"

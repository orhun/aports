# Contributor: Hoang Nguyen <folliekazetani@protonmail.com>
# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=gallery-dl
pkgver=1.25.1
pkgrel=0
pkgdesc="CLI tool to download image galleries"
url="https://github.com/mikf/gallery-dl"
arch="noarch"
license="GPL-2.0-or-later"
depends="py3-requests python3"
makedepends="py3-setuptools"
checkdepends="py3-pytest yt-dlp"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/mikf/gallery-dl/archive/v$pkgver.tar.gz"

build() {
	python3 setup.py build

	make man completion
}

check() {
	make test
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"

	# Install fish completion to the correct directory
	mv "$pkgdir"/usr/share/fish/vendor_completions.d "$pkgdir"/usr/share/fish/completions
}

sha512sums="
96f929af5cf70c7a89eca2ac8380cd7ba36ed806d062a6e339238ebfec2387aa4806d432cd28599b37382a69d7cdb33306069d41954e1421dff2523e2ae6a8df  gallery-dl-1.25.1.tar.gz
"

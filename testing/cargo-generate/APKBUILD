# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=cargo-generate
pkgver=0.18.1
pkgrel=1
pkgdesc="Use pre-existing git repositories as templates"
url="https://github.com/cargo-generate/cargo-generate"
license="MIT OR Apache-2.0"
arch="all !s390x" # fails to build nix crate
makedepends="cargo libgit2-dev openssl-dev"
source="https://github.com/cargo-generate/cargo-generate/archive/v$pkgver/cargo-generate-$pkgver.tar.gz
	remove-cargo-husky.patch
	"
options="net" # fetch dependencies

export CARGO_REGISTRIES_CRATES_IO_PROTOCOL="sparse"

case "$CARCH" in
riscv64)
	options="$options textrels"
	;;
esac

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo build --frozen --release --no-default-features
}

check() {
	cargo test --frozen --lib --no-default-features
}

package() {
	install -Dm755 target/release/$pkgname -t "$pkgdir"/usr/bin/
}

sha512sums="
045e8091c73d31d52b874cc1d96e8a7e6c31396f783c837e25917174f43ef2de5feb89ff0e112133134ec608d7c2707b5292afb2c6c31e53e1956131498af7e3  cargo-generate-0.18.1.tar.gz
58a1f0451adf67d5e4afc22d962dad90929ee845a36ce732f3415f6d398f2f2b88593eec16ef2dc8208ced95df7bd176a4d1a9f52c8d1c00885f143627f840d0  remove-cargo-husky.patch
"

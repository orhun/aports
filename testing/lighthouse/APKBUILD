# Contributor: Oleg Titov <oleg.titov@gmail.com>
# Maintainer: Oleg Titov <oleg.titov@gmail.com>
pkgname=lighthouse
pkgver=4.0.1
pkgrel=0
pkgdesc="Ethereum 2.0 Client"
url="https://lighthouse.sigmaprime.io/"
arch="x86_64 aarch64"  # limited by upstream
license="Apache-2.0"
makedepends="
	cargo
	clang15-dev
	cmake
	openssl-dev
	protobuf-dev
	zlib-dev
	"
options="net !check" # disable check as it takes too long
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/sigp/lighthouse/archive/v$pkgver/lighthouse-$pkgver.tar.gz"

# secfixes:
#   2.2.0-r0:
#     - CVE-2022-0778

export OPENSSL_NO_VENDOR=true
export RUSTFLAGS="$RUSTFLAGS -L /usr/lib/"
export CARGO_REGISTRIES_CRATES_IO_PROTOCOL="sparse"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo build --release \
		--frozen \
		--package lighthouse \
		--features "portable"
}

check() {
	cargo test --release --frozen \
		--workspace \
			--exclude ef_tests \
			--exclude eth1 \
			--exclude genesis
}

package() {
	install -D -m755 "target/release/lighthouse" "$pkgdir/usr/bin/lighthouse"

	install -Dm 644 -t "$pkgdir/usr/share/doc/lighthouse" README.md
}

sha512sums="
29d2064087990820ff36bc8e4def74da5c7f2fd9c2ab9a5580d2429306c0a3cde20781a2eac5107c019351d2326f1f32dca9cadab1545b41748ac9400939c666  lighthouse-4.0.1.tar.gz
"

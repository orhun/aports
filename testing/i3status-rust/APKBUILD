# Contributor: Galen Abell <galen@galenabell.com>
# Contributor: Maxim Karasev <begs@disroot.org>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=i3status-rust
pkgver=0.30.6
pkgrel=0
pkgdesc="i3status replacement in Rust"
url="https://github.com/greshake/i3status-rust"
arch="all !s390x !riscv64" # limited by cargo
license="GPL-3.0-only"
makedepends="
	cargo
	curl-dev
	dbus-dev
	lm-sensors-dev
	notmuch-dev
	openssl-dev>3
	pulseaudio-dev
	"
options="net"
provides="i3status-rs=$pkgver-r$pkgrel"
subpackages="$pkgname-doc"
source="
	https://github.com/greshake/i3status-rust/archive/refs/tags/v$pkgver/i3status-rust-v$pkgver.tar.gz
	https://dev.alpinelinux.org/archive/i3status-rs/i3status-rs-$pkgver.1
	"

export CARGO_REGISTRIES_CRATES_IO_PROTOCOL="sparse"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo build --release --frozen
}

check() {
	cargo test --all-features
}

package() {
	install -Dm755 target/release/i3status-rs -t "$pkgdir"/usr/bin/

	install -Dm644 "$srcdir"/i3status-rs-$pkgver.1 \
		"$pkgdir"/usr/share/man/man1/i3status-rs.1

	install -Dm644 files/themes/* -t "$pkgdir"/usr/share/i3status-rust/themes/
	install -Dm644 files/icons/* -t "$pkgdir"/usr/share/i3status-rust/icons/
}

sha512sums="
e66f381d03186f19863ca3f30d626a719976144cf7bd6d6f4028d5c88bfbd90bf144c25d019236a14fc7940a3b9fae880fe27d6e51b1b55fc2807251f41a5910  i3status-rust-v0.30.6.tar.gz
af2c6f8f531ffcd2c01d0bbf1ab69136ff64d48b1b30f22aea9941217070a377fa7157fe3dcb131331e4e340c503a2478aaa826a27aae690e097cdd9068db18d  i3status-rs-0.30.6.1
"

# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=py3-mitmproxy-wireguard
pkgver=0.1.21
pkgrel=0
pkgdesc="WireGuard frontend for mitmproxy"
url="https://github.com/decathorpe/mitmproxy_wireguard"
license="MIT"
arch="all !ppc64le !riscv64 !s390x" # fails to build ring crate
makedepends="cargo maturin py3-installer"
source="https://github.com/decathorpe/mitmproxy_wireguard/archive/$pkgver/py3-mitmproxy-wireguard-$pkgver.tar.gz"
builddir="$srcdir/mitmproxy_wireguard-$pkgver"

export CARGO_REGISTRIES_CRATES_IO_PROTOCOL="sparse"

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	maturin build --release --frozen --manylinux off
}

check() {
	cargo test --frozen
}

package() {
	python3 -m installer -d "$pkgdir" \
		target/wheels/mitmproxy_wireguard-*.whl
}

sha512sums="
fb4c072a6e5807d8f30f28bb1f15414b5a68cc302aa66f6e0ee5df1da391a26298f5efe82e99d62f4d44cebcad89c5631b2ab6b94f51c1f0dbc8fa485fd61f69  py3-mitmproxy-wireguard-0.1.21.tar.gz
"

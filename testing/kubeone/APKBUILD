# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=kubeone
pkgver=1.6.1
# Use the latest stable Kubernetes version at the time as the default
# https://dl.k8s.io/release/stable-1.26.txt
_k8sver=1.26.3
pkgrel=0
pkgdesc="Automate Kubernetes cluster operations on all platforms"
url="https://kubeone.io/"
arch="all"
license="Apache-2.0"
makedepends="go"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/kubermatic/kubeone/archive/refs/tags/v$pkgver.tar.gz"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	local _goldflags="
	-X k8c.io/kubeone/pkg/cmd.commit=AlpineLinux
	-X k8c.io/kubeone/pkg/cmd.version=$pkgver
	-X k8c.io/kubeone/pkg/cmd.date=$(date -u "+%Y-%m-%dT%H:%M:%SZ" ${SOURCE_DATE_EPOCH:+-d @$SOURCE_DATE_EPOCH})
	-X k8c.io/kubeone/pkg/cmd.defaultKubeVersion=$_k8sver
	-X k8c.io/kubeone/pkg/cmd.defaultCloudProviderName=none
	"

	go build -v -ldflags "$_goldflags"

	mkdir -p man
	./kubeone document man -o man/

	for shell in bash zsh; do
		./kubeone completion $shell > kubeone.$shell
	done
}

check() {
	go test ./pkg/... ./test/...
}

package() {
	install -Dm755 kubeone -t "$pkgdir"/usr/bin/

	install -Dm644 man/*.1 -t "$pkgdir"/usr/share/man/man1/

	install -Dm644 kubeone.bash \
		"$pkgdir"/usr/share/bash-completion/completions/kubeone
	install -Dm644 kubeone.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_kubeone
}

sha512sums="
c00aa91a986c1a1a3fda29b1656170bf628cb40dbca30f87b7eda3284b054fc8c42e1db5be9a3a1035aba5b5a761a695fac03a2d075e019ddc302ad0358ab6ef  kubeone-1.6.1.tar.gz
"
